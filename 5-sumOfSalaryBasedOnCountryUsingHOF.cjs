// 5. Find the sum of all salaries based on country using only HOF method

const jobsArr = require("./data/1-arrays-jobs.cjs");
function sumOfSalaryBasedOnCountryUsingHOF(jobsArr) {
    let salarySum = jobsArr.reduce((resultObj, item) => {
        if (resultObj[item.location] !== undefined) {
            resultObj[item.location] += Number(item.salary.slice(1));
        }
        else {
            resultObj[item.location] = Number(item.salary.slice(1));
        }
        return resultObj;
    }, {});
    return salarySum;
}
console.log(sumOfSalaryBasedOnCountryUsingHOF(jobsArr));

