// 1. Find all Web Developers

const jobsArr = require("./data/1-arrays-jobs.cjs");

function findAllWebDevelopers(jobsArr) {
    let allWebDevelopers = jobsArr.filter((item) => {
        if (item.job.includes("Web Developer")) {
            return item;
        }
    });
    return allWebDevelopers;
}
console.log(findAllWebDevelopers(jobsArr));
