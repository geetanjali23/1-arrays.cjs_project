//    6. Find the average salary of based on country using only HOF method
const jobsArr = require("./data/1-arrays-jobs.cjs");
function avgSalaryUsingHOF(jobsArr) {
    let salarySum = jobsArr.reduce((accumulator, item) => {
        if (accumulator[item.location] !== undefined) {
            accumulator[item.location][0] += Number(item.salary.slice(1));
            accumulator[item.location][1]++;
        }
        else {
            accumulator[item.location] = [Number(item.salary.slice(1))];
            accumulator[item.location][1] = 1;
        }
        return accumulator;
    }, {});
    for (let key in salarySum) {
        salarySum[key] = (salarySum[key][0] / salarySum[key][1]).toFixed(2);
    }
    return salarySum;

    // let jobsArr = {};
    // jobsArr.map((item) => {
    //     if (jobsArr[item.location] !== undefined) {
    //         jobsArr[item.location][0] += Number(item.salary.replaceAll("$", ""));;
    //         jobsArr[item.location][1] += 1;
    //     }
    //     else {
    //         jobsArr[item.location] = [];
    //         jobsArr[item.location][0] = Number(item.salary.replaceAll("$", ""));;
    //         jobsArr[item.location][1] = 1;
    //     }
    // });
    // for (let key in jobsArr) {
    //     jobsArr[key] = (jobsArr[key][0] / jobsArr[key][1]).toFixed(2);
    // }
    // return jobsArr;
}
console.log(avgSalaryUsingHOF(jobsArr), "Printing average salary");