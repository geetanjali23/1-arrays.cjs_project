// 4. Find the sum of all salaries 

const jobsArr = require("./data/1-arrays-jobs.cjs");
function sumOfSalary(jobsArr) {
    let salarySum = jobsArr.reduce((accumulator, arrObj) => {
        accumulator += Number(arrObj.salary.slice(1));
        return accumulator;//valid, undefined,invalid, null, empty
    }, 0);
    return salarySum.toFixed(2);

    // Solved using map function
    // let ans = 0;
    // jobsArr.map((item) => {
    //     ans += parseFloat(item.salary.replaceAll("$", ""));
    // });
    // return ans.toFixed(2);
}
console.log(sumOfSalary(jobsArr), "Printing sum of salary");
