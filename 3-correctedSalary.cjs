// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

const jobsArr = require("./data/1-arrays-jobs.cjs");
function addNewKeyOfCorrectedSalary(jobsArr) {
    jobsArr.map((item) => {
        item.correctedSalary = Number(parseFloat(item.salary.replaceAll("$", "")) * 10000).toFixed(2);
    });
    return jobsArr;
}

console.log(addNewKeyOfCorrectedSalary(jobsArr))

